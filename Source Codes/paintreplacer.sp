#pragma semicolon 1

#include <sdktools>
#include <sdkhooks>
#include <tf2attributes>
#include <tf2_stocks>

new Handle:cvar_PaintReplacerEnabled;

#define ApplyPaintRed TF2Attrib_SetByName(entity, "set item tint RGB", PaintRed)
#define ApplyPaintBlu TF2Attrib_SetByName(entity, "set item tint RGB 2", PaintBlu)
#define ApplyPaintBlu2 TF2Attrib_SetByName(entity, "set item tint RGB 2", PaintRed)

public Plugin myinfo = {
	name = "Paint Replacer", 
	author = "hotgrits", 
	description = "Replace paints with custom colors.", 
	version = "0.0.1", 
	url = ""
};


public OnPluginStart()
{
	cvar_PaintReplacerEnabled = CreateConVar("sm_paintreplacerenabled", "1", "Enable Paint Replacer?", _, true, 0.0, true, 1.0);
	HookEvent("post_inventory_application", Event_PostInventoryApplication);
}

public Action:Event_PostInventoryApplication(Handle:event, const String:name[], bool:dontBroadcast)
{
	if (!GetConVarBool(cvar_PaintReplacerEnabled)) { return; }

	new iEntity = -1;
	while( ( iEntity = FindEntityByClassname( iEntity, "tf_wearable" ) ) != -1 )
    {
       PaintLogic(iEntity);
    }
}

public PaintLogic(entity)
{
	//--- Obtain addresses for the paint color attributes ---//

	new Float:PaintRed = TF2Attrib_HookValueFloat(0.0, "set_item_tint_rgb", entity);
	new Float:PaintBlu = 0.0;

//--- Lots of if statements to handle the replacements ---//
	//The Bitter Taste of Defeat and Lime
	if (PaintRed == 3329330.0) { PaintRed = 6275935.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	//Pink as Hell
	if (PaintRed == 16738740.0) { PaintRed = 15109295.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	//Pinch of Sugar for Pink as Hell
	//if (PaintRed == 16738740.0) { PaintRed = 14911124.0; PaintBlu = 7844308.0; ApplyPaintRed; ApplyPaintBlu; return; }
	//A Distinctive Lack of Hue
	if (PaintRed == 1315860.0) { PaintRed = 2631720.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	//A Color Similar to Slate
	if (PaintRed == 3100495.0) { PaintRed = 3107156.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	//An Extraordinary Abundance of Tinge
	if (PaintRed == 15132390.0) { PaintRed = 12829635.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	//After Eight
	if (PaintRed == 2960676.0) { PaintRed = 2105370.0; ApplyPaintRed; ApplyPaintBlu2; return; }
	return;
}