#pragma semicolon 1

#include <sdktools>
#include <sdkhooks>
#include <tf2_stocks>

new Handle:cvar_PomsonSize;


public Plugin myinfo = {
	name = "Pomson Collision Size", 
	author = "hotgrits", 
	description = "Set the collision size of the Pomson 6000 projectile.", 
	version = "0.1", 
	url = ""
};


public OnPluginStart()
{
	cvar_PomsonSize = CreateConVar("sm_pomsonsize", "6.0", "Pomson 6000 projectile collision size, 1.99 cube by default. Rocket is about 7.5, arrow is about 5.5, syringe is about 1.5, rescue ranger is about 9.75.");
}

public OnEntityCreated(ent, const String:cls[])
{
	if (StrEqual(cls, "tf_projectile_energy_ring", true))
		{
		SDKHook(ent, SDKHook_Spawn, OnPomsonShotSpawned);
		}
	return;
}

public Action:OnPomsonShotSpawned(ent)
{
	SDKHook(ent, SDKHook_Think, OnPomsonShotThink_Hitbox);
	return;
}

public OnPomsonShotThink_Hitbox(ent)
{
	new Float:vecMins[3] = {-1.0,-1.0,-1.0};
	new Float:vecMaxs[3] = { 1.0, 1.0, 1.0};

	ScaleVector(vecMins, GetConVarFloat(cvar_PomsonSize) * 0.5);
	ScaleVector(vecMaxs, GetConVarFloat(cvar_PomsonSize) * 0.5);

	SetEntPropVector(ent, Prop_Send, "m_vecMins", vecMins);
	SetEntPropVector(ent, Prop_Send, "m_vecMaxs", vecMaxs);

	SDKUnhook(ent, SDKHook_Think, OnPomsonShotThink_Hitbox);

	return;
}