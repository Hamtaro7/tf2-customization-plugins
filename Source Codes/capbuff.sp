#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <tf2>

new Handle:tf_ctf_bonus_time = INVALID_HANDLE;

public OnPluginStart()
{	
	tf_ctf_bonus_time = FindConVar("tf_ctf_bonus_time");

	HookEvent("ctf_flag_captured", OnFlagCapture);
}

public Action:OnFlagCapture(Handle:event, const String:name[], bool:dontBroadcast)
{
//	new CappingTeam = GetEventInt(event, "capping_team");

	for (int i = 0; i < MaxClients; i++)
	{
//		if (!IsValidClient(i) || !IsPlayerAlive(i) || GetClientTeam(i) != CappingTeam)
		if (!IsValidClient(i) || !IsPlayerAlive(i))
		{
			continue;
		}

		TF2_AddCondition(i, TFCond_RegenBuffed, float(GetConVarInt(tf_ctf_bonus_time)));
	}
}

stock bool IsValidClient(int client, bool bAllowBots = false, bool bAllowDead = true)
{
    if (!(1 <= client <= MaxClients) || !IsClientInGame(client) || (IsFakeClient(client) && !bAllowBots) || IsClientSourceTV(client) || IsClientReplay(client) || (!bAllowDead && !IsPlayerAlive(client)))
    {
        return false;
    }
    return true;
} 