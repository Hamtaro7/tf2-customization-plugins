#pragma semicolon 1

#include <sdktools>
#include <sdkhooks>
#include <tf2_stocks>

new Handle:cvar_GrenadeSize;


public Plugin myinfo = {
	name = "Grenade Collision Size", 
	author = "hotgrits", 
	description = "Set the collision size of all grenades.", 
	version = "0.0.2", 
	url = ""
};


public OnPluginStart()
{
	cvar_GrenadeSize = CreateConVar("sm_grenadesize", "6.0", "Grenade hitbox size. Examples: Iron Bomber is 8.7, other grenades are 4.0, and front of rocket is 7.43)");
}

public OnEntityCreated(ent, const String:cls[])
{
	if (StrEqual(cls, "tf_projectile_pipe", true))
		{
		SDKHook(ent, SDKHook_Spawn, OnGrenadeShotSpawned);
		}
	return;
}

public Action:OnGrenadeShotSpawned(ent)
{
	SDKHook(ent, SDKHook_Think, OnGrenadeShotThink_Hitbox);
	return;
}

public OnGrenadeShotThink_Hitbox(ent)
{
//Don't bother, everything but the iron bomber is 4x4x4, and the iron bomber is 8.7x8.7x8.7, so just set them all the same.
//Stickies don't use the hull either so that won't matter.
//	GetEntPropVector(ent, Prop_Send, "m_vecMins", vecMins);
//	GetEntPropVector(ent, Prop_Send, "m_vecMaxs", vecMaxs);

	new Float:vecMins[3] = {-1.0,-1.0,-1.0};
	new Float:vecMaxs[3] = { 1.0, 1.0, 1.0};

	ScaleVector(vecMins, GetConVarFloat(cvar_GrenadeSize) * 0.5);
	ScaleVector(vecMaxs, GetConVarFloat(cvar_GrenadeSize) * 0.5);

	SetEntPropVector(ent, Prop_Send, "m_vecMins", vecMins);
	SetEntPropVector(ent, Prop_Send, "m_vecMaxs", vecMaxs);

	SDKUnhook(ent, SDKHook_Think, OnGrenadeShotThink_Hitbox);

	return;
}