#include <sdktools>
#include <tf2>

#pragma semicolon 1

public Plugin myinfo = {
	name = "Flip the Coin!", 
	author = "hotgrits", 
	description = "Crits or death.", 
	version = "0.1", 
	url = ""
};


public OnPluginStart()
{
	RegConsoleCmd("sm_ftc", Command_FlipTheCoin, "sm_ftc - Flip the coin!");
}

public Action:Command_FlipTheCoin(client, args)
{
	float FatesWill = GetURandomFloat();
	int Fate = FloatCompare(FatesWill, 0.5);

	char FlipperName[MAX_TARGET_LENGTH];
	GetClientName(client, FlipperName, sizeof(FlipperName));
	
	if (Fate >= 0)
	{	
		TF2_AddCondition(client, TFCond_CritCanteen, 5.0);

		PrintToChatAll("Fate smiles upon %s", FlipperName);
	}
	else
	{
		FakeClientCommand(client, "kill");

		PrintToChatAll("Fate frowns upon %s", FlipperName);
	}

	return Plugin_Handled;
}