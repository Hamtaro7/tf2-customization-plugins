#include <sdktools>
#include <sdkhooks>

#pragma semicolon 1

public Plugin myinfo = {
	name = "killvector", 
	author = "hotgrits", 
	description = "Recreation of killvector and explodevector.", 
	version = "0.1", 
	url = ""
};


public OnPluginStart()
{
	RegConsoleCmd("sm_killvector", Command_KillVector, "Kill yourself with forward and added vertical force. Values above 3000 are pointless, 0 can be used if no force is wanted. Add a 1 to explode. Usage: sm_killvector <velocity> <upwardvelocity> <optional:explode>");
}

public Action:Command_KillVector(client, args)
{
	if(!IsPlayerAlive(client))
	{
		ReplyToCommand(client, "Error: You must be alive to die.");
		return Plugin_Handled;
	}
	
	if(args < 2 || args > 3)
	{
		ReplyToCommand(client, "Usage: sm_killvector <velocity> <upwardvelocity> <optional:explode>");
		return Plugin_Handled;
	}
	
	new String:arg1[8], String:arg2[8];
	GetCmdArg(1, arg1, sizeof(arg1));
	GetCmdArg(2, arg2, sizeof(arg2));
	float killvectorVelocity = StringToFloat(arg1);
	float killvectorUpwardVelocity = StringToFloat(arg2);

	new Float:vecPlayerVelocity[3];
	new Float:vecPlayerEyes[3];

	GetClientEyeAngles(client, vecPlayerEyes);
	GetAngleVectors(vecPlayerEyes, vecPlayerEyes, NULL_VECTOR, NULL_VECTOR);
	
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", vecPlayerVelocity);

	NormalizeVector(vecPlayerEyes, vecPlayerEyes);
	vecPlayerVelocity[0] += vecPlayerEyes[0]*killvectorVelocity;
	vecPlayerVelocity[1] += vecPlayerEyes[1]*killvectorVelocity;
	vecPlayerVelocity[2] += vecPlayerEyes[2]*killvectorVelocity+killvectorUpwardVelocity;

	TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, vecPlayerVelocity);

	if(args == 3)
	{
		SDKHook(client, SDKHook_PostThinkPost, ExplodeVectorApply);
		return Plugin_Handled;
	}

	SDKHook(client, SDKHook_PostThinkPost, KillVectorApply);

	return Plugin_Handled;
}

public KillVectorApply(int client)
{
	SDKUnhook(client, SDKHook_PostThinkPost, KillVectorApply);
	FakeClientCommand(client, "kill");
}

public ExplodeVectorApply(int client)
{
	SDKUnhook(client, SDKHook_PostThinkPost, ExplodeVectorApply);
	FakeClientCommand(client, "explode");
}