#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <tf2>
#include <tf2_stocks>

#pragma semicolon 1

new Handle:cvarJarateDuration = INVALID_HANDLE;
//new Handle:cvarBleedDuration = INVALID_HANDLE;
new Handle:cvarMilkDuration = INVALID_HANDLE;
//new Handle:cvarGasDuration = INVALID_HANDLE;

public Plugin myinfo = {
	name = "Jar Duration Nerf",
	author = "hotgrits",
	description = "Jar effects last a different length of time.",
	version = "0.1",
	url = ""
};

int	m_Shared;

public OnPluginStart()
{
	if (!LookupOffset(m_Shared, "CTFPlayer", "m_Shared"))
		SetFailState("Could not look up offset for CTFPlayer::m_Shared!");

	cvarJarateDuration  = CreateConVar("jar_jarateduration", "10.0", "Jarate effect duration in seconds.");
//    cvarBleedDuration   = CreateConVar("jar_bleedduration", "10.0", "Bleed effect duration in seconds.");
	cvarMilkDuration    = CreateConVar("jar_milkduration", "10.0", "Mad Milk effect duration in seconds.");
//    cvarGasDuration     = CreateConVar("jar_gasduration", "10.0", "Gas effect duration in seconds.");
}

public void TF2_OnConditionAdded(client, TFCond:cond)
{
	if (cond == TFCond_Jarated)
	{
		TF2_SetConditionDuration(client, TFCond_Jarated, GetConVarFloat(cvarJarateDuration));
	}
//	if (cond == TFCond_Bleeding)
//	{
//		TF2_SetConditionDuration(client, TFCond_Bleeding, GetConVarFloat(cvarBleedDuration));
//	}
	if (cond == TFCond_Milked)
	{
		TF2_SetConditionDuration(client, TFCond_Milked, GetConVarFloat(cvarMilkDuration));
	}
//	if (cond == TFCond_Gas)
//	{
//		TF2_SetConditionDuration(client, TFCond_Gas, GetConVarFloat(cvarGasDuration));
//	}
	return;
}

stock float TF2_GetConditionDuration(const int client, const TFCond cond)
{
	if (!TF2_IsPlayerInCondition(client, cond))
		return 0.0;

	Address aCondSource   = view_as< Address >(LoadFromAddress(GetEntityAddress(client) + view_as< Address >(m_Shared + 8), NumberType_Int32));
	Address aCondDuration = view_as< Address >(view_as< int >(aCondSource) + (view_as< int >(cond) * 20) + (2 * 4));
	return view_as< float >(LoadFromAddress(aCondDuration, NumberType_Int32));
}

stock void TF2_SetConditionDuration(const int client, const TFCond cond, const float time)
{
	if (!TF2_IsPlayerInCondition(client, cond))
		return;

	Address aCondSource   = view_as< Address >(LoadFromAddress(GetEntityAddress(client) + view_as< Address >(m_Shared + 8), NumberType_Int32));
	Address aCondDuration = view_as< Address >(view_as< int >(aCondSource) + (view_as< int >(cond) * 20) + (2 * 4));
	StoreToAddress(aCondDuration, view_as< int >(time), NumberType_Int32);
}

stock bool LookupOffset(int &offset, const char[] classname, const char[] propname)
{
	return (offset = FindSendPropInfo(classname, propname)) > 0;
}