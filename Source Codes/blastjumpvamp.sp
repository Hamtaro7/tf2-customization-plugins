#pragma semicolon 1

#include <sdktools>
#include <sdkhooks>
#include <tf2>
#include <tf2_stocks>
#include <tf2attributes>

public Plugin myinfo =
{
	name = "Health on Radius Damage during Blast Jump",
	author = "hotgrits",
	description = "Gain health from dealing radius damage while blast jumping.",
	version = "0.1",
	url = ""
};

public void TF2_OnConditionAdded(client, TFCond:cond)
{
	if (cond == TFCond_BlastJumping)
	{
	TF2Attrib_SetByName(client, "health on radius damage", 10.0);
	}
	return;
}

public void TF2_OnConditionRemoved(client, TFCond:cond)
{
	if (cond == TFCond_BlastJumping)
	{
	TF2Attrib_SetByName(client, "health on radius damage", 0.0);
	}
	return;
}