#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <tf2>
#include <tf2_stocks>

#pragma semicolon 1

public Plugin myinfo = 
{
	name = "Wrangled Sentry Shield Adjuster",
	author = "hotgrits",
	description = "Adjusts the wrangled sentry shield damage protection strength.",
	version = "0.1",
	url = ""
}

public OnPluginStart()
{
	HookEvent("player_builtobject", PlayerBuiltObject);
}

public PlayerBuiltObject(Handle:event,const String:name[],bool:dontBroadcast)
{
	new entity = GetEventInt(event,"index");
	if(TF2_GetObjectType(entity) == TFObject_Sentry)
	{
		SDKHook(entity, SDKHook_OnTakeDamage, OnTakeDamage);
	}
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3]){
	if(attacker > 0 || attacker <= MAXPLAYERS)
	{
		int ShieldLevel = GetEntProp(victim, Prop_Send, "m_nShieldLevel");
		if (ShieldLevel > 0)
		{
			//By default it takes 33% damage with a shield, not sure if this is before or after OnTakeDamage.
			damage *= 1.5;//Undo the 33% damage effect and make it take double damage instead.
			return Plugin_Changed;	
		}
	}
	return Plugin_Continue;
} 